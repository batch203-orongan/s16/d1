// console.log("Hello World!");

// [SECTION] Arithmetic Operators

	let x = 1397;
	let y = 7831;

	// Addition
	let sum = x + y;
	console.log("Result of addition operator: " +sum);

	// Subtraction
	let differnce = x - y;
	console.log("Result of subtraction operator:" + differnce);

	// Multiplication
	let product = x*y;
	console.log("Result of multiplication operator:" +product);

	// Division
	let quotient = x / y;
	console.log("Result of division operator:" + quotient);

	// Modulo -> Remainder
	let modulo = x % y;
	console.log("Result of modulo operator: " +modulo);

// [SECTION] Assignment Operator (=)
	// Basic Assignment Operator (=)
	// The assignment operator assigns the value of the "right hand" oerand to a variable.
	let assignmentNumber = 8;
	console.log("The current value of the assignmentNumber variable: "+assignmentNumber);

	// Addition Assignment Operator
						//  8 + 2
	// assignmentNumber = assignmentNumber + 2; long method
	assignmentNumber += 2; // shorthand method
	console.log("Result of addition assignmentNumber operator: " +assignmentNumber);

	// Subtraction/Multiplication/Division (-=, *=, /=)
	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: "+assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: "+assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: "+assignmentNumber);

// [SECTION] PEMDAS (Order of Operations)
// Multiple Operators and Parenthesis
// when multiple operators are applied in a single statement it follows PEMDAS
/*
	The operation were done in the following orders
	1. 3 * 4 = 12 | 1 + 2 -12 / 5
	2. 12 / 5 = 2.4 | 1 + 2 - 2.4
	3. 1 + 2 = 3 | 3 - 2.4
	4. 3 - 2.4 = 0.6 //result

*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result od mdas opearation: " +mdas);
/*
	The operations were done in the following order:
	1. 4 / 5 = 0.8  1 + (2 - 3) * 0.8
	2. 2 - 3 = -1 | 1 + (-1) * 0.8
	3. -1 * 0.8 = -0.8
	4. 1 - 0.8 = 0.20 //result
*/
// let pmdas = 1 + (2 - 3) * (4/5); // .20
// grouping the operatiors using parenthesis will deliver different result.
let pmdas = (1 + (2 - 3)) * (4/5);
console.log("Result of pmdas opearation: " +pmdas); // 0

//[SECTION] Increment and Decrement
// Operators that add or subtract a value by 1 and reassign the value of the variable where the increment(++)/decrement(--) was applied.

	let z = 1;
	// The value of "z" is added by a value of one before returning the value and storing it in the variable.
	let increment = ++z
	console.log("Result of the pre-increment: "+increment);
	console.log("Result of pre-increment for z: "+z);
	// JavaScript read the code from top to bottom and left to right
	// The value of "z" is returned and stored in the varioable "increment" then the value "z" is increase by one
	increment = z++;
	// The value of "z" is at 2 before it was incremented
	console.log("Result of post-increment: "+increment);
	console.log("Result of the post-increment for z: "+z);

	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement for z: " +z);

	decrement = z--;
	console.log("Result of post-decrement:" +decrement);
	console.log("Result of post-decrement for z: "+z);

// [SECTION] Type Coercion
// is the automatic conversion values from one data type to another.

	let numA ='10';
	let numB = 12;

	// Coercion value it usually performs concatination if a string value is involved.
	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;

	let nonCoercion = numC + numD;
	console.log(nonCoercion); //30
	console.log(typeof nonCoercion);

	/*
		-The result is a number
		-The boolesn "true" is also associated with the value of 1.
	*/
	let numE = true + 1;
	console.log(numE);

	/*
		- The result is a number
		- The result "false" is also associated with the value of 0.
	*/
	numF = false + 1;
	console.log(numF);

// [SECTION] Comparison Operators
// Comparison operators are used to evaluate and compare the left and right operands.
// After evaluation, it returns a boolean value.

let juan = "juan";

// Equality Operator (==)
/*
	-Checks whether the operands are equal/have the same content.
	-Attempt to CONVERT AND COMPARE operands of different data type.
*/
	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1'); //true
	console.log(0 == false); //true
	// Compare two strings that are the same.
	console.log('juan' == 'juan'); //true
	// Compares a string with the variable juan
	console.log('juan' == juan); //true

// Inequality Operator (!=)
/*
	- Checks whether the operands are not equal/have the different value.
	- Attempts to CONVERT AND COMPARE operand of different data type.
*/

	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(0 != false); //false
	console.log('juan' != "juan"); //false
	console.log('juan' != juan); //false
// Strict Equality Operator (===)
// Check whether the operands are equal/have the same content.
// Also COMPARES the DATA TYPES of 2 values.

	console.log(1 === 1); //true
	console.log(1 === 2); //false
	console.log(1 === '1'); //false
	console.log(0 === false); //false
	console.log('juan' === 'juan'); //true
	console.log('juan' === juan); //true

// Strict Inequality Operator (!==)
/*
	- Checks whether the operands are not wqual/don't have the same content
	- Also COMPARES the SATA TYPES of 2 values.
*/

	console.log(1 !== 1); //false
	console.log(1 !== 2); //true
	console.log(1 !== '1'); //true
	console.log(0 !== false); //true
	console.log('juan' !== 'juan'); //false
	console.log('juan' !== juan); //false

// [SECTION] Relational Operators
// Some cpmparison operators check whether one calue is greater or less than to the other value.

let a = 50;
let b = 65;

// GT or Greater Than Operator (>)
let isGreaterThan = a > b; // 50 > 65 = false
console.log(isGreaterThan); //false

// LT or Less Than Operators
let isLessThan = a < b; //50 < 65 = true
console.log(isLessThan); //true

// GTE Greater Than or Equal ( >= )
let isGTOrEqual = a >= b; // 50 >= 65
console.log(isGTOrEqual); //false

// LTE or Less Than or Equal ( <= )
let isLTOrEqual = a <= b; // 50 <= 65
console.log(isLTOrEqual); //true

let numStr = "30";
console.log(a > numStr); //true - forced coercion to change the string to number.

let str = "twenty";
console.log(b >= str); //false
// Since the string is not numeric, the string was not converted to a number. 65 >= NAN (Not a Number).

// [SECTION] Logical Operators
// Logical Operators allow us to be more specific in th logical combination of conditions and evaluations. It return boolean.

let isLegatAge = true;
let isRegistered = false;
// Logical AND Operator(&& - Double Ampersand)
// Returns true if all operands are true.
let allRequirementsMet = isLegatAge && isRegistered;
console.log("Result of logical AND Operator: " + allRequirementsMet);

// Logical OR Operator (|| - Double Pipe)
// Return true if one of the operand are true.
let someRequirementsMet = isLegatAge || isRegistered;
console.log("Result of logical OR Operator: "+ someRequirementsMet);

// Logical Not Operator (! - Exclamation Point);
// Returns the opposite value
let someRequirementsNotMet = !isLegatAge;
console.log("Result of logical Not Operator: " + someRequirementsNotMet);
